using System.Collections.Generic;

namespace dotnet.Models
{
    public class Player
    {
        public User user {get; set;}

        public List<Card> Hand { get; set; }

        public List<Card> Discard { get; set;}

        public bool alive = true;

        public bool untargetable {get; set;}

        public Player(User user) {
            Hand = new List<Card>();
            Discard = new List<Card>();
            this.user = user;
            this.untargetable = false;
        }
    }
}