using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace dotnet.Models
{
    public class Game
    {

        [JsonIgnore]
        public List<Card> deck = new List<Card>();

        public List<Player> players = new List<Player>();

        public int activePlayer = 0;

        public int cardsInDeck { get => getCardsInDeck();}

        public GameStatus status = new GameStatus();
        
        public Game() {
            deck.Add(new Card(8,"Princess"));
            deck.Add(new Card(7,"Countess"));
            deck.Add(new Card(6,"King"));
            deck.Add(new Card(5,"Prince"));
            deck.Add(new Card(5,"Prince"));
            deck.Add(new Card(4,"Handmaiden"));
            deck.Add(new Card(4,"Handmaiden"));
            deck.Add(new Card(3,"Baron"));
            deck.Add(new Card(3,"Baron"));
            deck.Add(new Card(2,"Priest"));
            deck.Add(new Card(2,"Priest"));
            deck.Add(new Card(1,"Guard"));
            deck.Add(new Card(1,"Guard"));
            deck.Add(new Card(1,"Guard"));
            deck.Add(new Card(1,"Guard"));
            deck.Add(new Card(1,"Guard"));
            deck.Shuffle();
            Player player1 = new Player(User.CECILE);
            Player player2 = new Player(User.OLIVIER);
            player1.Hand.Add(drawCard());
            player1.Hand.Add(drawCard());
            player2.Hand.Add(drawCard());
            players.Add(player1);
            players.Add(player2);

            for (int i = 0; i < 12; i++)
            {
                Move move = new Move(0,0,0);
                try {
                    playMove(move);
                }
                catch (InvalidTargetException e) {
                    Console.WriteLine(e.Message);
                }
            }
        }

        private Card drawCard() {
            return deck.Pop();
        }

        private Player getActivePlayer() {
            return players[activePlayer]; 
        }

        private int remainingPlayers() {
            return players.FindAll(player => player.alive).Count;
        }

        public int getCardsInDeck() {
            return deck.Count;
        }

        private void passTurn() {
            if (remainingPlayers() <= 1) {
                status = new GameStatus();
                status.over = true;
                return;
            }
            int candidate = (activePlayer + 1) % players.Count;
            int nextPlayer = -1;
            while (candidate != activePlayer) {
                if (players[candidate].alive) {
                    nextPlayer = candidate;
                    break;
                }
                candidate = (candidate + 1) % players.Count;
            }

            activePlayer = nextPlayer;
            getActivePlayer().untargetable = false;
            getActivePlayer().Hand.Add(drawCard());
        }

        private Player getTarget(int target) {
            try {
                Player trg = players[target];
                if (!trg.alive) {
                    throw new InvalidTargetException();
                }

                if (trg.untargetable) {
                    throw new InvalidTargetException();
                }

                return trg;
            }
            catch (Exception e) {
                throw new InvalidTargetException();
            }
        }

        private void kill(Player player) {
            player.alive = false;
            player.Discard.AddRange(player.Hand);
            player.Hand.Clear();
        }

        private void discard(Player player, int card) {
            Card discarded = player.Hand[card];
            if (discarded.power == 8) {
                kill(player);
            }
            else {
                player.Hand.RemoveAt(card);
                player.Discard.Add(discarded);
            }
        }

        public bool playMove(Move move) {
            if (status.over) {
                return false;
            }

            Player player = getActivePlayer();
            if (player.Hand.Count != 2) {
                return false;
            }

            int playedCardIndex = move.card;

            if (playedCardIndex > 1 || playedCardIndex < 0) {
                return false;
            }
            
            Card playedCard = player.Hand[playedCardIndex];
            int otherCardIndex = (playedCardIndex + 1) % 2;
            Card otherCard = player.Hand[otherCardIndex];

            Player target;
            
            if (otherCard.power == 7 && (playedCard.power == 5 || playedCard.power == 6)) {
                throw new InvalidMoveException();
            }
            
            switch (playedCard.power)
            {
                case 1:
                if (move.target == activePlayer) {
                    throw new InvalidTargetException();
                }
                
                if (move.extra < 2 || move.extra > 8) {
                    throw new InvalidMoveException();
                }

                target = getTarget(move.target);
                if (target.Hand[0].power == move.extra) {
                    kill(target);
                }
                break;
                case 3:
                if (move.target == activePlayer) {
                    throw new InvalidTargetException();
                }
                target = getTarget(move.target);
                if (otherCard.power > target.Hand[0].power) {
                    kill(target);
                }
                else if (otherCard.power < target.Hand[0].power) {
                    kill(player);
                }
                break;
                case 4:
                player.untargetable = true;
                break;
                case 5:
                target = getTarget(move.target);
                int cardToDiscard = 0;
                if (move.target == activePlayer) {
                    cardToDiscard = otherCardIndex;
                }
                discard(target,cardToDiscard);
                if (target.alive) {
                    target.Hand.Add(drawCard());
                }
                break;
                case 6:
                if (move.target == activePlayer) {
                    throw new InvalidTargetException();
                }
                target = getTarget(move.target);
                Card newCard = target.Hand.Pop();
                player.Hand[(playedCardIndex + 1) % 2] = newCard;
                target.Hand.Add(otherCard);
                break;
                case 7:
                break;
                case 8:
                kill(player);
                break;
                default:
                //return false;
                break;
            }

            if (player.alive) {
                player.Hand.RemoveAt(playedCardIndex);
                player.Discard.Add(playedCard);
            }
            
            passTurn();
            return true;
        }

        public class GameStatus {
            public bool over = false;
        }

        public class InvalidTargetException : Exception {

        }

        public class InvalidMoveException : Exception {

        }
        
    }


}