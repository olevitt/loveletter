namespace dotnet.Models
{
    public class Card
    {
        public int power = 8;

        public string name = "PRINCESS";

        public Card(int power, string name) {
            this.power = power;
            this.name = name;
        }
    }
}