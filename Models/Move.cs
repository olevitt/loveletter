namespace dotnet.Models
{
    public class Move
    {
        public int card {get; set;}

        public int target;

        public int extra;

        public Move() {

        }

        public Move(int card, int target, int extra) {
            this.card = card;
            this.target = target;
            this.extra = extra;
        }
    }
}