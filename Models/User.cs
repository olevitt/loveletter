namespace dotnet.Models
{
    public class User
    {
        public static readonly User OLIVIER = new User("OLIVIER"), CECILE = new User("CECILE");
         public string name { get; set;}

         public User(string name) {
             this.name = name;
         }
    }
}