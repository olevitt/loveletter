﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using dotnet.Models;
using Flurl.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using TodoApi.Models;

namespace dotnet.Controllers
{

   

    [Route("api/game")]
    public class GameController : Controller
    {
        static Game game;

        [HttpGet]
        public Game Get()
        {
            return game;
        }

        [HttpGet("create")]
        public Game create() {
            game = new Game();
            
            WebsocketController.BroadcastMessage(JsonConvert.SerializeObject(game));
            return Get();
        }

        [HttpPost("play")]
        public Game play([FromBody] Move move) {
            WebsocketController.BroadcastMessage("New move "+move.card);
            bool result = game.playMove(move);
            return Get();
        }

       

        [HttpGet("{id}")]
        public async Task<Object> GetAsync(int id, string toto)
        {

            /*var dbConnection = new SqliteConnection("Data Source=MyDatabase.sqlite");
            dbConnection.Open();
            string sql = "CREATE TABLE highscores (name VARCHAR(20), score INT)";
            SqliteCommand command = new SqliteCommand(sql, dbConnection);
            await command.ExecuteNonQueryAsync();

            sql = "insert into highscores (name, score) values ('Me', 3000)";
            command = new SqliteCommand(sql, dbConnection);
            command.ExecuteNonQuery();
            sql = "insert into highscores (name, score) values ('Myself', 6000)";
            command = new SqliteCommand(sql, dbConnection);
            command.ExecuteNonQuery();
            sql = "insert into highscores (name, score) values ('And I', 9001)";
            command = new SqliteCommand(sql, dbConnection);
            command.ExecuteNonQuery();

            var results = new SqliteCommand("select * from highscores order by score desc",dbConnection).ExecuteReader();
            while (results.Read()) {
                Console.WriteLine(results.GetString(0));
            }*/
            var responseString = await "https://httpbin.org/ip".GetAsync();
            Console.WriteLine(responseString.Headers);
            Console.WriteLine(toto);
            return responseString.Headers;
        }
    }
}
