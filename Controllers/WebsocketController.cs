using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace dotnet.Controllers
{
    public class WebsocketController
    {
        public static List<WebSocket> clients = new List<WebSocket>();
         public static void BroadcastMessage(String data) {
            clients.ForEach(websocket => SendString(websocket, data, CancellationToken.None));
        }


        private static Task SendString(WebSocket ws, String data, CancellationToken cancellation)
        {
            var encoded = Encoding.UTF8.GetBytes(data);
            var buffer = new ArraySegment<Byte>(encoded, 0, encoded.Length);
            return ws.SendAsync(buffer, WebSocketMessageType.Text, true, cancellation);
        }

        internal static async Task newWebsocket(WebSocket webSocket)
        {
            try {
            clients.Add(webSocket);
            await Echo(webSocket);
            }
            catch (Exception e) {
                Console.Error.WriteLine(e);
            }
        }

         private static async Task Echo(WebSocket webSocket)
            {
                var buffer = new byte[1024 * 4];
                WebSocketReceiveResult result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                while (!result.CloseStatus.HasValue)
                {
                    result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                }
                clients.Remove(webSocket);
                await webSocket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
            }
    }
}